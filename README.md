# Abstract:
This is a summary of findings for the underlying security of Visual Voicemail (VVM). If an attacker hijacks/MITM's a victim’s SMS for a short period of time (e.g. hijack the SIM card, MITM the cell tower, rogue App with SMS access), an attacker is able to gain user access to a victim's Visual Voicemail (VVM) through the carrier leaking the VVM IMAP server credentials via SMS.

After initial exploitation, SMS/SIM card access is no longer needed for persistent access. An attacker can now remotely access a victim’s VVM and has the same access to VVM as the victim. This attack is often completely stealthy (i.e. the victim will have no idea on an Android/iOS OS, and will only be presented with a cryptic SMS on mainline mobile Linux devices), and it is extremely difficult/impossible for the victim to change VVM credentials to prevent unauthorized access.

The author fully tested a Proof of Concept on T-Mobile USA and AT&T USA. While the author has not fully tested a PoC with Verizon Wireless USA, but the same PoC can test the security of Verizon Wireless USA to see how this carrier is effected. MVNOs typically use the same infrastructure for VVMs as the parent network, so any MVNOs will likely have the same vulnerabilities.

When attempting responsible disclosure with AT&T and T-Mobile USA via HackerONE, the author was told this was not a ["concrete and exploitable risk"](https://gitlab.com/kop316/vvm-disclosure/-/blob/main/Screenshot_at_2021-06-28_06-30-07.png). Verizon Wireless USA has a bug bounty program via HackerONE, and their VVM feature is only available to postpaid customers. As such, the author did not attempt a PoC with Verizon Wireless (VZW). Additionally, the author worked with CERT/CC to coordinate disclosure of this vulnerability. As of the time of writing, no vendor has attempted to reach out to the author or CERT/CC.

## CVE
This is filed under CVE-2022-23835.

## CERT/CC
This is filed under Case VU#383864.

# Background:
Visual Voicemail (VVM) was first introduced by iOS. It allows a mobile phone to download and present the voicemails in a program such as [this](https://gitlab.com/kop316/vvmplayer) rather than dialing a number and listening to the voicemails serially. Android added support for it in Android 6.0. There is an open standard for VVM that is primarily governed by [OMTP](https://www.gsma.com/newsroom/wp-content/uploads/2012/07/OMTP_VVM_Specification_1_3.pdf). However, as this document details, many carriers introduce their own tweaks to VVM.

Visual voicemail is a combination of SMS and an IMAP [1]. SMS provides a command and control architecture for VVM. A client can  activate, deactivate, and request VVM status via SMS to the carrier (more info is shown [here](https://gitlab.com/kop316/vvm-disclosure/-/blob/main/README.md#vvm-sms-command-and-control)). The carrier also sends sync messages to a client (e.g. the carrier will send the client an SMS when a new VVM arrive).

A client actually retrieves/manages VVMs though an IMAP server (more info is shown [here](https://gitlab.com/kop316/vvm-disclosure/-/blob/main/README.md#vvm-imap-server)).

## VVM SMS Command and Control
All client VVM SMS messages are sent to a specific number that handles VVM SMSes (called in this paper the "VVM Destination Number"). The three types of client originated SMSes are:

- Activation: request activation of VVM for a client number,
- Deactivation: request deactivation of VVM for a client number, and
- Status: request status of the VVM subscription for a client number.

When a client sends any sort of STATUS SMS (activate, deactivate, status), the carrier will respond with all credentials needed to log into the IMAP server (i.e. username, password, server host-name). From [1], Section 2.9.1.6, a status message from the carrier to the client will have a structure similar to:

```
//VVM:STATUS:st=N;rc=0;srv=1:10.115.67.251;tui=123;dn=999;ipt=143;spt=25;u=78236487@wirelesscarrier.com;pw=32u4yguetrr34;lang=eng|fre;g_len=25;vs_len=15;pw_len=4-6;smtp_u=super_user@wirelesscarrier.com;smtp_pw=48769463wer;pm=Y;gm=N;vtc=D;vt=1
```
Key attributes:
- u=78236487@wirelesscarrier.com -> username
- pw=32u4yguetrr34 -> password
- srv=1:10.115.67.251 -> IMAP server to access

### T-Mobile USA SMS Command and Control Implimentation
On T-Mobile, SMS Command and Control is a slight deviation from [1], in that the client-originated STATUS messages are:

- Activate: `Activate:dt=6`
- Deactivate: `Deactivate:dt=6`
- Status: `STATUS:dt=6`

The VVM Destination Number is `127`, and carrier response will be in the following format:

```
//VVM:STATUS:st=R;rc=0;srv=e3.vvm.mstore.msg.t-mobile.com;ipt=148;u=12065550100;pw=secure_pw;lang=1|2|3|4;g_len=180;vs_len=10;pw_len=4-9”
```
Key attributes:
- u=12065550100 -> username
- pw=secure_pw -> password
- srv=e3.vvm.mstore.msg.t-mobile.com -> IMAP server to access

### AT&T USA SMS Command and Control Implimentation
AT&T obfuscates their VVM authentication, but otherwise follow the same VVM protocol. The VVM Destination Number is `94183567`. On Android, AT&T's VVM is only supported through their proprietary application. Upon setup/status request, the Android client sends the following SMS ([presumably the activation/status SMS](https://gitlab.com/kop316/vvm-disclosure/-/blob/main/Screenshot_20210624-145935_Messaging_sanitized.png)):

```
GET?c=ATTV:Pixel 3a/11:4.4.0.10183&v=1.0&l=2065550100&AD
```
Key attributes:
- Pixel 3a -> Model Number
- 11 -> Android 11
- 4.4.0.10183 -> AT&T Visual Voicemail
- Version 1.0 -> Version of VVM (Maybe? I am not sure)
- 2065550100 -> Own number

The author did not find a way to unsubscribe from VVM through the AT&T Android VVM application.

The carrier response will be in the form:
```
vvm.mobile.att.net:5400?f=0&v=1010&m=2065550100&p=XFNXW1JUUlFcWTc2NTQ4&P=XFNXW1JUUlFcWTc2NTQ4&S=I&s=5433&i=143/993&t=4:2065550100:A:CMSGROUP024121413
```
Key attributes:
- m=2065550100 -> username
- p=XFNXW1JUUlFcWTc2NTQ4 -> password
- vvm.mobile.att.net -> IMAP server


The AT&T password is numerical only (7-15 numbers), is obfuscated in 2 steps:

1) The password is Base64 encoded, and,
2) there is a substitution cipher for the first 10 numerical values of the password (it is plaintext for the last 5):

Lookup table for Password (based on position):
```
Decoded     | Position
            | 1 2 3 4 5 6 7 8 9 10 11-15
----------------------------------------
0           | X T Q ^ Z S U U _ Y  00000
1	    | Y U P _ [ R T T ^ X  11111
2	    | Z V S \ X Q W W ] [  22222
3	    | [ W R ] Y P V V \ Z  33333
4	    | \ P U Z ^ W Q Q [ ]  44444
5	    | ] Q T [ _ V P P Z \  55555
6	    | ^ R W X \ U S S Y _  66666
7	    | _ S V Y ] T R R X ^  77777
8	    | P \ Y V R [ ] ] W Q  88888
9	    | Q ] X W S Z \ \ V P  99999
```

The numerical substitution depends on the number’s position in the password. For example: a `1` in the first position will be `Y`, but a `1` in the fifth position will be an `R`, a `7` in the third position will be `V`, but it will be an `X` for the ninth position.

An example de-obfuscation:
- `Base64_decode(XFNXW1JUUlFcWTc2NTQ4)=\SW[RTRQ\Y76548`
- `Lookup_table(\SW[RTRQ\Y76548)=476587743176548`

An implementation for decoding the password can be found [here](https://gitlab.com/kop316/vvmd/-/blob/main/src/vvmutil.c#L430).

NOTE: The password deobfuscation method may have changed, however this obfuscation was correct as of July 2021.

### Version Wireless USA SMS Command and Control Implementation

Version Wireless (VZW) USA uses a slightly modified form of OMTP that is referred to as `vvm3`. The VVM Destination Number is `900080006200`. Upon setup/status request, the client sends the following SMS:

```
STATUS
```
The carrier response will be in the form:
```
//VZWVVM:STATUS:st=R;rc=0;srv=ch1lv.imsvm.com;ipt=143;u=redacted@vzwazc.com;pw=redacted;lang=1,2,3,4,5,6,7,8;g_len=60;vs_len=10;pw_len=4-7;dev_t=5;vmg_url=https://vmg.vzw.com/VMGIMS/VMServices
```

Key attributes:
- u=redacted@vzwazc.com -> username
- pw=redacted -> password
- srv=ch1lv.imsvm.com
- vmg_url=https://vmg.vzw.com/VMGIMS/VMServices

The URL may be needed in order to initially activate VVM for an account, but it is not necessary once VVM is already activated. Additionally, there is no way to [deactivate VVM on VZW USA](https://cs.android.com/android/platform/superproject/+/master:packages/apps/Dialer/java/com/android/voicemail/impl/protocol/Vvm3Protocol.java;drc=80f3d35f64a720a1d59552589cb77e2061eadf6d;l=101).

## VVM IMAP server
The VVM IMAP server is a standard IMAP server. All VVMs are really emails that contain attachments. [1] section 2.10.3 shows an example VVM voice message:

```
Return-Path: <>
Received: from msuic1 (10.106.145.31) by MIPS.SITE1 (MIPS Email Server)
id 45879DD300000196 for 11210@vi.com; Tue, 19 Dec 2006 12:12:09 +0200
subject: voice mail
MIME-Version: 1.0 (Voice Version 2.0)
Message-Id: <31.24.2326006@msu31_24>
Content-Type: Multipart/ voice-message; boundary="------------Boundary-00=_90NIQYRXFQQMYJ0CCJD0"
From: 771004@vi.com
To: 771000@vi.com
Content-Duration: 17
Message-Context: voice-message
Date: Tue, 19 Dec 2006 10:12:09 +0000 (UTC)
--------------Boundary-00=_90NIQYRXFQQMYJ0CCJD0
Content-Type: Text/Plain
Content-Transfer-Encoding: 7bit
click on attachment
--------------Boundary-00=_90NIQYRXFQQMYJ0CCJD0
Content-Type: audio/amr
Content-Transfer-Encoding: base64
Content-Disposition: attachment; filename="vm.amr"
Content-Duration: 17
[message attachment]
--------------Boundary-00=_90NIQYRXFQQMYJ0CCJD0--
```

In the above example: `vm.amr` is the actual voicemail message (The Attachment is not contained for brevity, but it will be base64 encoded).

Other examples of voicemails can be found [here](https://gitlab.com/kop316/vvmd/-/tree/main/unit).

### AT&T and T-Mobile's VVM IMAP Server
AT&T uses standard IMAP SSL via port 993 to access VVM (Authentication: Digest-MD5). T-Mobile uses standard IMAP SSL via port 993 to access VVM (Authentication: Plain). Both carrier’s IMAP servers listens on the open internet and authentication is via username and password, so a SIM card/SMS access is not needed to log into the VVM IMAP server.

### VZW's VVM IMAP Server
VZW USA only allows access to their IMAP server via their mobile network, so a SIM card for VZW (or its MVNO) is required to access VZW VVMs.

# Exploitation
Here are a few example exploitation scenarios to gain access to VVM credentials:

### Malicious Application
While using the Android AT&T VVM Application, it asked for access to SMS. After this, it sent the activation SMS. Though there was no user notification of the received SMS (this seems to be intentional behavior, certain messages do not show up in the AOSP SMS/MMS messaging application), the VVM Application nonetheless received the necessary credentials and logged in. Additionally, the author was able to subscribe and unsubscribe from T-Mobile by sending the Activation/Deactivation SMS to the T-Mobile VVM Destination Number through the AOSP SMS/MMS messaging application. Based on this pattern of behavior, the author believes that a malicious application with SMS permissions on the Android platform can access/eavesdrop the VVM credentials on a victim client, or send an SMS to gain access to the VVM credentials.

The author is unfamiliar with iOS, and is unfamiliar with the application permissions on iOS. Thus, the author does not know if a standard iOS application can access SMS in the same way. However, the author suspects that spyware similar to [Pegasus](https://en.wikipedia.org/wiki/Pegasus_(spyware)) would be able (and very likely) captures VVM credentials.

Mainline mobile Linux distributions with the Modem Manager stack treat VVM SMSes as an ordinary text SMS, and SMS access can be limited by distributions. Some distributions (e.g. postmarketOS) limit SMS read/write access via Polkit. However, many mainline mobile Linux distributions do not limit such access, so applications with user access are able to gain SMS permissions and gain access to VVM credentials. `vvmd` stores VVM credentials in an ordinary text file in the user's home folder, so anyone with access to a user's home folder can steal the VVM credentials.

### Rogue Cell Tower
As SMS has no client authentication, so the author assumes that a rogue cell tower (e.g. a stingray) can MITM SMS to gain access the VVM credentials.

### SIM Hijacking/"Evil Maid" attack
A malicious actor can hijack the SIM card for a minute (really, the time is limited by swapping the SIM and the round-trip of two SMS messages) and will have access to these credentials.

# Stealthiness
In many of the above exploit scenarios, the initial exploit can happen without any knowledge of the victim. The SMS Command and control architecture is supposed to be transparent to a user, and as such, is intentionally hidden from a user. While the carrier may log such SMS activity, there is no way for a user to check such behavior (through the phone or through looking at carrier SMS usage).

### Carrier usage Web Portal
None of the activation, deactivation, nor status SMSes appear under AT&T/T-Mobile usage web portal. The author would be surprised to hear that VZW USA shows such usage in their web portal.

### Android
Any SMS sent/received by the AOSP dialer does not show up in the AOSP Message app. On Android, the STATUS SMS that leaks the credentials do not appear on the messaging app, and the AT&T VVM app says it will send SMSes and to ignore it. In the case of AT&T and T-Mobile, there is additionally no carrier logs of the SMS activity visible to the user.

### IPhone
None of the activation, deactivation, status SMSes appear in the iOS iMessage application.

### Pinephone Modem Manager stack
All of the status SMSes will show up on the Pinephone Modem Manager stack as a regular SMS (e.g. they will be visible in Chatty, mmcli, etc.). However, unless a user is familiar with VVM SMSes, the user will likely not be aware of what is occurring.

# Persistence (Changing the VVM Password)
To the author's knowledge, T-Mobile does not change the VVM password for an account, nor does it have a mechanism to change it. It would not surprise the author if the VVM password for their account has not changed since they opened the account. AT&T only changes the VVM password if a user manually changes it within their Android VVM application.

The author does not know how often VZW USA changes the VVM password, but saw no mechanism in Android/iOS to change it.

### AT&T/T-Mobile Website
The author found no way to change the VVM password on the carrier's websites.

### IMEI Change (T-Mobile/AT&T)
The author never saw the VVM password change if the IMEI associated with the SIM card changes (despite the carrier knowing that the IMEI changed).

### IPhone
The author did not find a way to change the VVM password on an IPhone.

### Android AOSP Dialer
The author did not find a way to change the VVM password on the Android AOSP dialer.

### AT&T Android VVM Application
AT&T Android VVM application allows you to change the VVM password (numerical, 7-15 digits).

### vvmd
vvmd does not support changing a VVM password.

# Impact
After initial exploitation, the VVM IMAP server is accessible over the internet (though it may require access to the carrier's mobile internet, as in VZW's case), and only requires username and password access. Specific SIM card/SMS access is no longer needed to access a victim's VVM. The malicious actor has full access to the victim’s VVM. There is no indication on to a victim that someone else has access to their VVM. Android leaves their VVMs on the IMAP server until the client deletes it, so any VVMs on the client are accessible to a malicious actor. `vvmd` deletes all VVMs after processing to minimize the risk of other's downloading the VVM. The IPhone's behavior is unknown to the author.

# MVNOs
MVNOs typically reuse the same VVM infrastructure as their parent carrier, so any findings related to a parent VVM structure will generally apply to the MVNO.

# Proof of Concept
[vvmd](https://gitlab.com/kop316/vvmd/) contains logic to process/capture T-Mobile USA, AT&T USA, and VZW USA VVM credentials (as well as MVNOs), as it is a client to access VVM for mainline mobile Linux devices.

# Disclosure

- June 21, 2021: Discovery
- June 24, 2021: Author disclosed findings to T-Mobile (via their HackerONE bug bounty program).
- June 25, 2021: Author disclosed findings to AT&T (via their HackerONE bug bounty program).
- June 28, 2021: [HackerONE closed the issue on AT&T](https://gitlab.com/kop316/vvm-disclosure/-/blob/main/Screenshot_at_2021-06-28_06-30-07.png).
- June 28, 2021: Author disclosed findings to CERT/CC.
- June 29, 2021: CERT/CC accepted the case. Public Disclosure: Feb 17, 2022.

# Vendor Response

When attempting responsible disclosure with AT&T and T-Mobile USA via HackerONE, the author was told this was not a ["concrete and exploitable risk"](https://gitlab.com/kop316/vvm-disclosure/-/blob/main/Screenshot_at_2021-06-28_06-30-07.png). Verizon Wireless USA has a bug bounty program via HackerONE, and their VVM feature is only available to postpaid customers. As such, the author did not attempt a PoC with Verizon Wireless (VZW). Additionally, the author worked with CERT/CC to coordinate disclosure of this vulnerability. As of the time of writing, no vendor has attempted to reach out to the author or CERT/CC.

# Note
All usernames/passwords referenced in this document are fictitious.

# References
[1] https://www.gsma.com/newsroom/wp-content/uploads/2012/07/OMTP_VVM_Specification_1_3.pdf

# License
This work is licensed Attribution 4.0 International (CC BY 4.0) https://creativecommons.org/licenses/by/4.0/

# Acknowledgments
Thank you Paul Simon, [Dr. Ken Mai](https://engineering.cmu.edu/directory/bios/mai-kenneth.html), and [Dr. Shawn Blanton](https://engineering.cmu.edu/directory/bios/blanton-shawn.html) for listening to my thoughts and helping me think through this issue. Thank you [CERT/CC](https://www.kb.cert.org/vuls/) for your help. Thank you [Clayton "craftyguy" Craft](https://freeradical.zone/@craftyguy) for your help editing this document!
